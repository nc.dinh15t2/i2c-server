import dotenv from 'dotenv';
dotenv.config();

export const MongoConfig = {
    URL: process.env.MONGO_URL,
    SCHEMA: process.env.SCHEMA
}

export const MySqlConfig = {
    HOST: process.env.DB_HOST,
    PORT: parseInt(process.env.DB_PORT),
    USER: process.env.DB_USER,
    PASS: process.env.DB_PASS,
    SCHEMA: process.env.DB_SCHEMA
}

export const PORT = parseInt(process.env.PORT);
export const SECRET_KEY = process.env.SECRET_KEY;
export const DEBUG_MODE = process.env.DEBUG_MODE === 'true';
export const LOG_DIR = process.env.LOG_DIR;