import path from 'path';
import { createLogger, format, transports } from 'winston';
import 'winston-daily-rotate-file';
import { LOG_DIR } from './config.util';

export const logger = createLogger({
    format: format.combine(
        format.splat(),
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        // format.colorize(),
        format.printf((log) => {
            if (log.stack) return `[${log.timestamp}] [${log.level}] ${log.stack}`;
            return `[${log.timestamp}] [${log.level}] ${log.message}`;
        }),
    ),
    transports: [
        new transports.Console(),
        // new transports.File({
        //     level: 'error',
        //     filename: 'errors.log'
        // }),
        new transports.DailyRotateFile({
            filename: 'application-%DATE%.log',
            dirname: LOG_DIR,
            datePattern: 'YYYY-MM-DD-HH',
            zippedArchive: true,
            maxSize: '20m',
            maxFiles: '14d'
        })
    ],
})