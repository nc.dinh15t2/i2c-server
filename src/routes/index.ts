import { Router } from 'express';
import { UserControllerV1 } from '../controllers';
import { ImageControllerV1 } from '../controllers/image_v1.controller';

const apiRouter = Router();

//v1
const routerV1 = Router();
routerV1.get('/users', UserControllerV1.list);
routerV1.post('/users', UserControllerV1.create);

routerV1.get('/images', ImageControllerV1.list);
routerV1.post('/images', ImageControllerV1.create);

apiRouter.use('/1.0', routerV1);



export default apiRouter;