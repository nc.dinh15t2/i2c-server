import mongoose from 'mongoose';
import { MongoConfig } from '../utils';

(async () => {
    await mongoose.connect(MongoConfig.URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        dbName: MongoConfig.SCHEMA
    });

    console.log('MongoDB connected');
})();