import { prop, getModelForClass } from '@typegoose/typegoose';

export class Image {
    @prop({
        maxlength: 3
    })
    public url: string;
}

export const ImageModel = getModelForClass(Image);