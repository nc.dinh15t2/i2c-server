import { Connection, createConnection, Repository } from "typeorm";
import { MySqlConfig } from "../utils";
import { User } from "./user.model";

let connection: Connection;
let _userRepo;

export function userRepo(): Repository<User> {
    return _userRepo;
}

(async () => {
    connection = await createConnection({
        type: 'mysql',
        host: MySqlConfig.HOST,
        port: MySqlConfig.PORT,
        username: MySqlConfig.USER,
        password: MySqlConfig.PASS,
        database: MySqlConfig.SCHEMA,
        synchronize: true,
        entities: [
            User
        ]
    });

    _userRepo = await connection.getRepository(User);
    console.log('Connected to mysql');
})();
