import { DEBUG_MODE } from "../utils";

export class BaseException extends Error {
    private status: number;
    private errorCode: string;
    private internalMessage: string;

    constructor(status, message, errorCode, internalMessage) {
        super(message);
        this.status = status;
        this.errorCode = errorCode;
        this.internalMessage = internalMessage;
    }
    
    public getReponse() {
        const resp = {
            status: this.status,
            message: this.message,
            error_code: this.errorCode
        };

        if(DEBUG_MODE) {
            resp['internal_message'] = this.internalMessage;
        }

        return resp;
    }

    public getStatus() {
        return this.status;
    }
}