import { BaseException } from "./base.exception";

export class NotfoundException extends BaseException {
    constructor(internalMessage = 'Not found') {
        super(404, 'Not found', '0001', internalMessage);
    }
}

export class UnauthorizedException extends BaseException {
    constructor(internalMessage = 'Unauthorized') {
        super(401, 'Unauthorized', '0002', internalMessage);
    }
}

export class ForbiddenException extends BaseException {
    constructor(internalMessage = 'Forbidden') {
        super(403, 'Forbidden', '0003', internalMessage);
    }
}

export class BadRequestException extends BaseException {
    constructor(message, code = '0000', internalMessage = 'Some thing wrong') {
        super(400, message, code, internalMessage);
    }
}