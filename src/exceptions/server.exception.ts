import { BaseException } from "./base.exception";

export class InternalServerException extends BaseException {
    constructor(code = '9999', internalMessage = 'Something wrong') {
        super(500, 'Something wrong', code, internalMessage);
    }
}