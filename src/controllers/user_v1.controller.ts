import { BadRequestException } from "../exceptions";
import { User, userRepo } from "../models";

export module UserControllerV1 {
    export async function list(req, res, next) {
        try {
            res.json(await userRepo().find());
        } catch (err) {
            next(err);
        }
    }

    export async function create(req, res, next) {
        try {
            const user = new User();
            user.firstName = 'Dinh';
            await userRepo().save(user);
            res.json(user);
        } catch (err) {
            next(err);
        }
    }
};