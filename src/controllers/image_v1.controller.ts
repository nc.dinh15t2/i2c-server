import { Image, ImageModel } from "../models";

export module ImageControllerV1 {
    export async function list(req, res, next) {
        try {
            res.json(await ImageModel.find());
        } catch(err) {
            next(err);
        }
    }
    export async function create(req, res, next) {
        try {
            const image = {
                url: 'http://'
            } as Image;
            res.json(await ImageModel.create(image));
        } catch(err) {
            next(err);
        }
    }
}