const express = require('express');
import { Application, Response } from 'express';
import { PORT } from './src/utils';
import apiRouter from './src/routes';
import { errorHandler, loggerHandler } from './src/middlewares';

const app: Application = express();

app.get('/check-app-version', (req, res) => {
    res.json({
        version: '1.0.0'
    });
});

app.use(loggerHandler);

app.use('/api', apiRouter);
app.use(errorHandler);

const server = app.listen(PORT, () => {
    console.log(`Listening on port: ${PORT}`);
});
